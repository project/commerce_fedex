<?php

namespace Drupal\commerce_fedex;

use FedexRest\Services\Rates\CreateRatesRequest;

/**
 * Manage FedEx API services.
 *
 * @package Drupal\commerce_fedex
 */
interface FedExRequestInterface {

  public const RATE_REQUEST_TYPE_LIST = 'LIST';

  public const RATE_REQUEST_TYPE_ACCOUNT = 'ACCOUNT';

  public const RATE_REQUEST_TYPE_PREFERRED = 'PREFERRED';

  public const RATE_REQUEST_TYPE_INCENTIVE = 'INCENTIVE';

  /**
   * Gets a new rate request.
   *
   * @param array $configuration
   *   The plugin configuration.
   *
   * @return \FedexRest\Services\Rates\CreateRatesRequest
   *   The rate request.
   */
  public function getRateRequest(array $configuration): CreateRatesRequest;

  /**
   * Gets the auth token.
   *
   * @param array $configuration
   *   The Plugin Configuration.
   *
   * @return string
   *   The token.
   *
   * @throws \Drupal\commerce_fedex\Exception\AuthorizationException
   */
  public function getToken(array $configuration): string;

}
