<?php

namespace Drupal\commerce_fedex\Plugin\Commerce\FedEx;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use FedexRest\Entity\Item;

/**
 * Defines the base interface for FedEx Service Plugins.
 *
 * @package Drupal\commerce_fedex\Plugin\Commerce\FedEx
 */
interface FedExPluginInterface extends ConfigurableInterface, PluginFormInterface, PluginInspectionInterface {

  /**
   * Adjust a package based on the items, shipment and profile.
   *
   * @param \FedexRest\Entity\Item $package
   *   The package to adjust.
   * @param array $shipment_items
   *   An array of shipment items.
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment.
   *
   * @return \FedexRest\Entity\Item
   *   The adjusted package.
   */
  public function adjustPackage(Item $package, array $shipment_items, ShipmentInterface $shipment): Item;

  /**
   * Function splitPackage.
   *
   * @param array $shipment_items
   *   An Array of shipment items.
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The Shipment.
   *
   * @return array
   *   An array of arrays of shipment items.
   */
  public function splitPackage(array $shipment_items, ShipmentInterface $shipment): array;

}
