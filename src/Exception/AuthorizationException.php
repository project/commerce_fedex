<?php

namespace Drupal\commerce_fedex\Exception;

/**
 * Authorization exception.
 */
class AuthorizationException extends \Exception {

  /**
   * {@inheritDoc}
   */
  public function __construct($message = '', $code = 0, ?\Throwable $previous = NULL) {
    if (empty($message)) {
      $message = 'Token not returned';
    }
    parent::__construct($message, $code, $previous);
  }

}
