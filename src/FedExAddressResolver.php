<?php

namespace Drupal\commerce_fedex;

use Drupal\address\AddressInterface;
use FedexRest\Entity\Address as FedexRestAddress;

/**
 * Allow Country level modifications when converting Drupal addresses to FedEx.
 */
trait FedExAddressResolver {

  /**
   * Resolve Addresses in Australia.
   *
   * @param \Drupal\address\AddressInterface $address
   *   The address to resolve.
   *
   * @return \FedexRest\Entity\Address
   *   The address for FedEx.
   */
  public static function addressResolveAu(AddressInterface $address): FedexRestAddress {

    $provinces = [
      'NSW' => 'NS',
      'NT' => 'NT',
      'QL' => 'QL',
      'SA' => 'SA',
      'TAS' => 'TS',
      'VIC' => 'VI',
      'WA' => 'WA',
    ];

    $fedex_rest_address = new FedexRestAddress();

    $street_lines = array_filter([
      $address->getAddressLine1(),
      $address->getAddressLine2(),
    ]);
    $fedex_rest_address->setStreetLines(...$street_lines);
    $fedex_rest_address->setCity($address->getLocality());
    $fedex_rest_address->setPostalCode($address->getPostalCode());
    $fedex_rest_address->setCountryCode($address->getCountryCode());
    $fedex_rest_address->setStateOrProvince($provinces[$address->getAdministrativeArea()]);

    return $fedex_rest_address;
  }

  /**
   * Resolve Addresses in Hong Kong.
   *
   * @param \Drupal\address\AddressInterface $address
   *   The address to resolve.
   *
   * @return \FedexRest\Entity\Address
   *   The address for FedEx.
   */
  public static function addressResolveHk(AddressInterface $address): FedexRestAddress {

    $fedex_rest_address = new FedexRestAddress();

    $street_lines = array_filter([
      $address->getAddressLine1(),
      $address->getAddressLine2(),
    ]);
    $fedex_rest_address->setStreetLines(...$street_lines);
    $fedex_rest_address->setCity($address->getAdministrativeArea() . ', ' . $address->getLocality());
    $fedex_rest_address->setPostalCode($address->getPostalCode());
    $fedex_rest_address->setCountryCode($address->getCountryCode());

    return $fedex_rest_address;
  }

  /**
   * Resolve Addresses in Barbados.
   *
   * @param \Drupal\address\AddressInterface $address
   *   The address to resolve.
   *
   * @return \FedexRest\Entity\Address
   *   The address for FedEx.
   */
  public static function addressResolveBb(AddressInterface $address): FedexRestAddress {

    $fedex_rest_address = new FedexRestAddress();

    $street_lines = array_filter([
      $address->getAddressLine1(),
      $address->getAddressLine2(),
    ]);
    $fedex_rest_address->setStreetLines(...$street_lines);
    $fedex_rest_address->setCity($address->getAdministrativeArea());
    $fedex_rest_address->setPostalCode($address->getPostalCode());
    $fedex_rest_address->setCountryCode($address->getCountryCode());

    return $fedex_rest_address;
  }

  /**
   * Resolve Addresses in Ireland.
   *
   * @param \Drupal\address\AddressInterface $address
   *   The address to resolve.
   *
   * @return \FedexRest\Entity\Address
   *   The address for FedEx.
   */
  public static function addressResolveIe(AddressInterface $address): FedexRestAddress {

    $provinces = [
      // cspell:disable
      'Co. Carlow' => 'N0',
      'Co. Cavan' => 'N1',
      'Co.  Clare' => 'N2',
      'Co. Cork' => ' N3',
      'Co.  Donegal' => 'N4',
      'Co. Dublin' => 'N5',
      'Co. Galway' => 'N6',
      'Co. Kerry' => 'N7',
      'Co. Kildare' => 'N8',
      'Co. Kilkenny' => 'N9',
      'Co. Laois' => 'NA',
      'Co. Leitrim' => 'NG',
      'Co. Limerick' => 'NI',
      'Co. Longford' => 'NK',
      'Co. Louth' => 'NL',
      'Co. Mayo' => 'NM',
      'Co. Meath' => 'NO',
      'Co. Monaghan' => 'NP',
      'Co. Offaly' => 'NQ',
      'Co. Roscommon' => 'NR',
      'Co. Sligo' => 'NW',
      // @todo Why are there duplicate keys?
      // Commenting out the first, since PHP keeps the last
      // when there are duplicates.
      // 'Co. Tipperary' => 'NY',
      'Co. Tipperary' => 'NX',
      'Co. Waterford' => 'NZ',
      'Co. Westmeath' => '10',
      'Co. Wexford' => '11',
      'Co. Wicklow' => '12',
      // cspell:enable
    ];

    $fedex_rest_address = new FedexRestAddress();

    $street_lines = array_filter([
      $address->getAddressLine1(),
      $address->getAddressLine2(),
    ]);
    $fedex_rest_address->setStreetLines(...$street_lines);
    $fedex_rest_address->setCity($address->getAdministrativeArea());
    $fedex_rest_address->setPostalCode($address->getPostalCode());
    $fedex_rest_address->setCountryCode($address->getCountryCode());
    $fedex_rest_address->setStateOrProvince($provinces[$address->getAdministrativeArea()]);

    return $fedex_rest_address;
  }

}
