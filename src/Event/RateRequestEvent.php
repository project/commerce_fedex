<?php

namespace Drupal\commerce_fedex\Event;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use FedexRest\Services\Rates\CreateRatesRequest;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Defines the rate request event for FedEx.
 *
 * @see \Drupal\commerce_fedex\Event\CommerceFedExEvents
 */
class RateRequestEvent extends Event {

  /**
   * The rate request object.
   *
   * @var \FedexRest\Services\Rates\CreateRatesRequest
   */
  protected $rateRequest;

  /**
   * The shipment being requested.
   *
   * @var \Drupal\commerce_shipping\Entity\ShipmentInterface
   */
  protected $shipment;

  /**
   * Constructs a new RateRequestEvent object.
   *
   * @param \FedexRest\Services\Rates\CreateRatesRequest $rate_request
   *   The rate request object.
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment being requested.
   */
  public function __construct(CreateRatesRequest $rate_request, ShipmentInterface $shipment) {
    $this->rateRequest = $rate_request;
    $this->shipment = $shipment;
  }

  /**
   * Gets the rate request object.
   *
   * @return \FedexRest\Services\Rates\CreateRatesRequest
   *   The rate request object.
   */
  public function getRateRequest(): CreateRatesRequest {
    return $this->rateRequest;
  }

  /**
   * Sets the rate request object.
   *
   * @param \FedexRest\Services\Rates\CreateRatesRequest $rate_request
   *   The rate request object.
   *
   * @return $this
   */
  public function setRateRequest(CreateRatesRequest $rate_request): self {
    $this->rateRequest = $rate_request;
    return $this;
  }

  /**
   * Gets the shipment.
   *
   * @return \Drupal\commerce_shipping\Entity\ShipmentInterface
   *   The shipment.
   */
  public function getShipment(): ShipmentInterface {
    return $this->shipment;
  }

  /**
   * Sets the shipment.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment.
   *
   * @return $this
   */
  public function setShipment(ShipmentInterface $shipment): self {
    $this->shipment = $shipment;
    return $this;
  }

}
