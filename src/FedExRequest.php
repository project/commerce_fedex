<?php

namespace Drupal\commerce_fedex;

use Drupal\commerce_fedex\Exception\AuthorizationException;
use FedexRest\Authorization\Authorize;
use FedexRest\Services\Rates\CreateRatesRequest;

/**
 * Manage FedEx API services.
 *
 * @package Drupal\commerce_fedex
 */
class FedExRequest implements FedExRequestInterface {

  /**
   * {@inheritdoc}
   */
  public function getToken(array $configuration): string {
    $auth = (new Authorize())
      ->setClientId($configuration['api_information']['api_key'])
      ->setClientSecret($configuration['api_information']['api_password']);

    if ($this->getMode($configuration) === 'live') {
      $auth->useProduction();
    }
    $response = $auth->authorize();
    if (!is_object($response)) {
      throw new AuthorizationException($response);
    }
    if (empty($response->access_token)) {
      throw new AuthorizationException();
    }
    return $response->access_token;
  }

  /**
   * {@inheritdoc}
   */
  public function getRateRequest(array $configuration): CreateRatesRequest {
    $rate_request = (new CreateRatesRequest())
      ->setAccessToken($this->getToken($configuration))
      ->setAccountNumber($configuration['api_information']['account_number'])
      ->setPickupType($configuration['options']['pickup_type']);

    if ($rate_request_types = array_values($configuration['options']['rate_request_type'])) {
      $rate_request->setRateRequestTypes(...$rate_request_types);
    }

    if ($this->getMode($configuration) === 'live') {
      $rate_request->useProduction();
    }

    return $rate_request;
  }

  /**
   * Gets the mode to use to connect.
   *
   * @param array $configuration
   *   The configuration array.
   *
   * @return string
   *   The mode (test or live).
   */
  protected function getMode(array $configuration): string {
    return $configuration['api_information']['mode'] ?: 'test';
  }

}
