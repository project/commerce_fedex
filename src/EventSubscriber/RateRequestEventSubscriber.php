<?php

namespace Drupal\commerce_fedex\EventSubscriber;

use Drupal\commerce_fedex\Event\CommerceFedExEvents;
use Drupal\commerce_fedex\Event\RateRequestEvent;
use FedexRest\Entity\Address;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribes to beforeRateRequest event.
 */
class RateRequestEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      CommerceFedExEvents::BEFORE_RATE_REQUEST => 'beforeRateRequest',
    ];
  }

  /**
   * Alters params before request in test mode.
   *
   * @param \Drupal\commerce_fedex\Event\RateRequestEvent $event
   *   The event.
   */
  public function beforeRateRequest(RateRequestEvent $event): void {
    $rate_request = $event->getRateRequest();
    if ($rate_request->production_mode) {
      return;
    }

    // Alter the request's params for the Test mode to receive response.
    // Without the alterations in Test mode the response can contain the error:
    // The response is unavailable for the request payload sent to this
    // virtualized sandbox instance. Please use the exact request payload
    // from API JSON Collection available for download through FedEx Developer
    // Portal and try again.
    // More info about Test mode can be found there:
    // https://developer.fedex.com/api/en-ca/guides/sandboxvirtualization.html
    /** @var \FedexRest\Entity\Item $item */
    foreach ($rate_request->getLineItems() as $item) {
      $item->setItemDescription('');
      $item->setGroupPackageCount(NULL);
      $item->setSequenceNumber(NULL);
      $item->setSubPackagingType('');
    }
    /** @var \FedexRest\Entity\Address $shipper_address */
    $shipper_address = $rate_request->getShipper()->address;
    $new_shipper_address = new Address();
    $new_shipper_address->setCountryCode($shipper_address->country_code);
    $new_shipper_address->setPostalCode($shipper_address->postal_code);
    $rate_request->getShipper()->withAddress($new_shipper_address);

    /** @var \FedexRest\Entity\Address $recipient_address */
    $recipient_address = $rate_request->getRecipient()->address;
    $new_recipient_address = new Address();
    $new_recipient_address->setCountryCode($recipient_address->country_code);
    $new_recipient_address->setPostalCode($recipient_address->postal_code);
    $rate_request->getRecipient()->withAddress($new_recipient_address);

    $rate_request->setPreferredCurrency('');
    $rate_request->setTotalPackageCount(0);
    $rate_request->setRateRequestTypes('ACCOUNT', 'LIST');
  }

}
