<?php

namespace Drupal\commerce_fedex_test;

use FedexRest\Services\Rates\CreateRatesRequest;

/**
 * A class to simulate a FedEx Rate Request.
 */
class TestRateRequest extends CreateRatesRequest {

  /**
   * {@inheritdoc}
   */
  public function request() {
    return $this->allInOneResults();
  }

  /**
   * Return a simulated valid response.
   *
   * @return object
   *   a simulated valid response.
   */
  private function allInOneResults(): object {
    // cspell:disable
    $json = <<<'EOD'
{
    "transactionId": "APIF_SV_RATC_TxID2e113732-aea4-4d83-a5eb-edc62e105f8c",
    "output":{"alerts":[{"code":"VIRTUAL.RESPONSE","message":"This is a Virtual Response.","alertType":"NOTE"}],"rateReplyDetails": [
            {
                "serviceType": "FIRST_OVERNIGHT",
                "serviceName": "FedEx First Overnight®",
                "packagingType": "YOUR_PACKAGING",
                "ratedShipmentDetails": [
                    {
                        "rateType": "LIST",
                        "ratedWeightMethod": "ACTUAL",
                        "totalDiscounts": 0.0,
                        "totalBaseCharge": 118.58,
                        "totalNetCharge": 124.51,
                        "totalNetFedExCharge": 124.51,
                        "shipmentRateDetail": {
                            "rateZone": "06",
                            "dimDivisor": 139,
                            "fuelSurchargePercent": 5.0,
                            "totalSurcharges": 5.93,
                            "totalFreightDiscount": 0.0,
                            "surCharges": [
                                {
                                    "type": "FUEL",
                                    "description": "Fuel Surcharge",
                                    "amount": 5.93
                                }
                            ],
                            "pricingCode": "PACKAGE",
                            "totalBillingWeight": {
                                "units": "LB",
                                "value": 2.0
                            },
                            "currency": "USD",
                            "rateScale": "14"
                        },
                        "ratedPackages": [
                            {
                                "groupNumber": 0,
                                "effectiveNetDiscount": 0.0,
                                "packageRateDetail": {
                                    "rateType": "PAYOR_ACCOUNT_PACKAGE",
                                    "ratedWeightMethod": "ACTUAL",
                                    "baseCharge": 118.58,
                                    "netFreight": 118.58,
                                    "totalSurcharges": 5.93,
                                    "netFedExCharge": 124.51,
                                    "totalTaxes": 0.0,
                                    "netCharge": 124.51,
                                    "totalRebates": 0.0,
                                    "billingWeight": {
                                        "units": "LB",
                                        "value": 2.0
                                    },
                                    "totalFreightDiscounts": 0.0,
                                    "surcharges": [
                                        {
                                            "type": "FUEL",
                                            "description": "Fuel Surcharge",
                                            "amount": 5.93
                                        }
                                    ],
                                    "currency": "USD"
                                }
                            }
                        ],
                        "currency": "USD"
                    },
                    {
                        "rateType": "ACCOUNT",
                        "ratedWeightMethod": "ACTUAL",
                        "totalDiscounts": 0.0,
                        "totalBaseCharge": 118.58,
                        "totalNetCharge": 124.51,
                        "totalNetFedExCharge": 124.51,
                        "shipmentRateDetail": {
                            "rateZone": "06",
                            "dimDivisor": 139,
                            "fuelSurchargePercent": 5.0,
                            "totalSurcharges": 5.93,
                            "totalFreightDiscount": 0.0,
                            "surCharges": [
                                {
                                    "type": "FUEL",
                                    "description": "Fuel Surcharge",
                                    "amount": 5.93
                                }
                            ],
                            "pricingCode": "PACKAGE",
                            "totalBillingWeight": {
                                "units": "LB",
                                "value": 2.0
                            },
                            "currency": "USD",
                            "rateScale": "14"
                        },
                        "ratedPackages": [
                            {
                                "groupNumber": 0,
                                "effectiveNetDiscount": 0.0,
                                "packageRateDetail": {
                                    "rateType": "PAYOR_LIST_PACKAGE",
                                    "ratedWeightMethod": "ACTUAL",
                                    "baseCharge": 118.58,
                                    "netFreight": 118.58,
                                    "totalSurcharges": 5.93,
                                    "netFedExCharge": 124.51,
                                    "totalTaxes": 0.0,
                                    "netCharge": 124.51,
                                    "totalRebates": 0.0,
                                    "billingWeight": {
                                        "units": "LB",
                                        "value": 2.0
                                    },
                                    "totalFreightDiscounts": 0.0,
                                    "surcharges": [
                                        {
                                            "type": "FUEL",
                                            "description": "Fuel Surcharge",
                                            "amount": 5.93
                                        }
                                    ],
                                    "currency": "USD"
                                }
                            }
                        ],
                        "currency": "USD"
                    }
                ],
                "operationalDetail": {
                    "ineligibleForMoneyBackGuarantee": false,
                    "astraDescription": "1ST OVR",
                    "airportId": "SWF",
                    "serviceCode": "06"
                },
                "signatureOptionType": "SERVICE_DEFAULT",
                "serviceDescription": {
                    "serviceId": "EP1000000006",
                    "serviceType": "FIRST_OVERNIGHT",
                    "code": "06",
                    "names": [
                        {
                            "type": "long",
                            "encoding": "utf-8",
                            "value": "FedEx First Overnight®"
                        },
                        {
                            "type": "long",
                            "encoding": "ascii",
                            "value": "FedEx First Overnight"
                        },
                        {
                            "type": "medium",
                            "encoding": "utf-8",
                            "value": "FedEx First Overnight®"
                        },
                        {
                            "type": "medium",
                            "encoding": "ascii",
                            "value": "FedEx First Overnight"
                        },
                        {
                            "type": "short",
                            "encoding": "utf-8",
                            "value": "FO"
                        },
                        {
                            "type": "short",
                            "encoding": "ascii",
                            "value": "FO"
                        },
                        {
                            "type": "abbrv",
                            "encoding": "ascii",
                            "value": "FO"
                        }
                    ],
                    "serviceCategory": "parcel",
                    "description": "First Overnight",
                    "astraDescription": "1ST OVR"
                }
            },
            {
                "serviceType": "PRIORITY_OVERNIGHT",
                "serviceName": "FedEx Priority Overnight®",
                "packagingType": "YOUR_PACKAGING",
                "ratedShipmentDetails": [
                    {
                        "rateType": "LIST",
                        "ratedWeightMethod": "ACTUAL",
                        "totalDiscounts": 0.0,
                        "totalBaseCharge": 87.58,
                        "totalNetCharge": 91.96,
                        "totalNetFedExCharge": 91.96,
                        "shipmentRateDetail": {
                            "rateZone": "06",
                            "dimDivisor": 139,
                            "fuelSurchargePercent": 5.0,
                            "totalSurcharges": 4.38,
                            "totalFreightDiscount": 0.0,
                            "surCharges": [
                                {
                                    "type": "FUEL",
                                    "description": "Fuel Surcharge",
                                    "amount": 4.38
                                }
                            ],
                            "pricingCode": "PACKAGE",
                            "totalBillingWeight": {
                                "units": "LB",
                                "value": 2.0
                            },
                            "currency": "USD",
                            "rateScale": "1574"
                        },
                        "ratedPackages": [
                            {
                                "groupNumber": 0,
                                "effectiveNetDiscount": 0.0,
                                "packageRateDetail": {
                                    "rateType": "PAYOR_ACCOUNT_PACKAGE",
                                    "ratedWeightMethod": "ACTUAL",
                                    "baseCharge": 87.58,
                                    "netFreight": 87.58,
                                    "totalSurcharges": 4.38,
                                    "netFedExCharge": 91.96,
                                    "totalTaxes": 0.0,
                                    "netCharge": 91.96,
                                    "totalRebates": 0.0,
                                    "billingWeight": {
                                        "units": "LB",
                                        "value": 2.0
                                    },
                                    "totalFreightDiscounts": 0.0,
                                    "surcharges": [
                                        {
                                            "type": "FUEL",
                                            "description": "Fuel Surcharge",
                                            "amount": 4.38
                                        }
                                    ],
                                    "currency": "USD"
                                }
                            }
                        ],
                        "currency": "USD"
                    },
                    {
                        "rateType": "ACCOUNT",
                        "ratedWeightMethod": "ACTUAL",
                        "totalDiscounts": 0.0,
                        "totalBaseCharge": 87.58,
                        "totalNetCharge": 91.96,
                        "totalNetFedExCharge": 91.96,
                        "shipmentRateDetail": {
                            "rateZone": "06",
                            "dimDivisor": 139,
                            "fuelSurchargePercent": 5.0,
                            "totalSurcharges": 4.38,
                            "totalFreightDiscount": 0.0,
                            "surCharges": [
                                {
                                    "type": "FUEL",
                                    "description": "Fuel Surcharge",
                                    "amount": 4.38
                                }
                            ],
                            "pricingCode": "PACKAGE",
                            "totalBillingWeight": {
                                "units": "LB",
                                "value": 2.0
                            },
                            "currency": "USD",
                            "rateScale": "1574"
                        },
                        "ratedPackages": [
                            {
                                "groupNumber": 0,
                                "effectiveNetDiscount": 0.0,
                                "packageRateDetail": {
                                    "rateType": "PAYOR_LIST_PACKAGE",
                                    "ratedWeightMethod": "ACTUAL",
                                    "baseCharge": 87.58,
                                    "netFreight": 87.58,
                                    "totalSurcharges": 4.38,
                                    "netFedExCharge": 91.96,
                                    "totalTaxes": 0.0,
                                    "netCharge": 91.96,
                                    "totalRebates": 0.0,
                                    "billingWeight": {
                                        "units": "LB",
                                        "value": 2.0
                                    },
                                    "totalFreightDiscounts": 0.0,
                                    "surcharges": [
                                        {
                                            "type": "FUEL",
                                            "description": "Fuel Surcharge",
                                            "amount": 4.38
                                        }
                                    ],
                                    "currency": "USD"
                                }
                            }
                        ],
                        "currency": "USD"
                    }
                ],
                "operationalDetail": {
                    "ineligibleForMoneyBackGuarantee": false,
                    "astraDescription": "P1",
                    "airportId": "SWF",
                    "serviceCode": "01"
                },
                "signatureOptionType": "SERVICE_DEFAULT",
                "serviceDescription": {
                    "serviceId": "EP1000000002",
                    "serviceType": "PRIORITY_OVERNIGHT",
                    "code": "01",
                    "names": [
                        {
                            "type": "long",
                            "encoding": "utf-8",
                            "value": "FedEx Priority Overnight®"
                        },
                        {
                            "type": "long",
                            "encoding": "ascii",
                            "value": "FedEx Priority Overnight"
                        },
                        {
                            "type": "medium",
                            "encoding": "utf-8",
                            "value": "FedEx Priority Overnight®"
                        },
                        {
                            "type": "medium",
                            "encoding": "ascii",
                            "value": "FedEx Priority Overnight"
                        },
                        {
                            "type": "short",
                            "encoding": "utf-8",
                            "value": "P-1"
                        },
                        {
                            "type": "short",
                            "encoding": "ascii",
                            "value": "P-1"
                        },
                        {
                            "type": "abbrv",
                            "encoding": "ascii",
                            "value": "PO"
                        }
                    ],
                    "serviceCategory": "parcel",
                    "description": "Priority Overnight",
                    "astraDescription": "P1"
                }
            },
            {
                "serviceType": "STANDARD_OVERNIGHT",
                "serviceName": "FedEx Standard Overnight®",
                "packagingType": "YOUR_PACKAGING",
                "ratedShipmentDetails": [
                    {
                        "rateType": "LIST",
                        "ratedWeightMethod": "ACTUAL",
                        "totalDiscounts": 0.0,
                        "totalBaseCharge": 85.83,
                        "totalNetCharge": 90.12,
                        "totalNetFedExCharge": 90.12,
                        "shipmentRateDetail": {
                            "rateZone": "06",
                            "dimDivisor": 139,
                            "fuelSurchargePercent": 5.0,
                            "totalSurcharges": 4.29,
                            "totalFreightDiscount": 0.0,
                            "surCharges": [
                                {
                                    "type": "FUEL",
                                    "description": "Fuel Surcharge",
                                    "amount": 4.29
                                }
                            ],
                            "pricingCode": "PACKAGE",
                            "totalBillingWeight": {
                                "units": "LB",
                                "value": 2.0
                            },
                            "currency": "USD",
                            "rateScale": "1371"
                        },
                        "ratedPackages": [
                            {
                                "groupNumber": 0,
                                "effectiveNetDiscount": 0.0,
                                "packageRateDetail": {
                                    "rateType": "PAYOR_ACCOUNT_PACKAGE",
                                    "ratedWeightMethod": "ACTUAL",
                                    "baseCharge": 85.83,
                                    "netFreight": 85.83,
                                    "totalSurcharges": 4.29,
                                    "netFedExCharge": 90.12,
                                    "totalTaxes": 0.0,
                                    "netCharge": 90.12,
                                    "totalRebates": 0.0,
                                    "billingWeight": {
                                        "units": "LB",
                                        "value": 2.0
                                    },
                                    "totalFreightDiscounts": 0.0,
                                    "surcharges": [
                                        {
                                            "type": "FUEL",
                                            "description": "Fuel Surcharge",
                                            "amount": 4.29
                                        }
                                    ],
                                    "currency": "USD"
                                }
                            }
                        ],
                        "currency": "USD"
                    },
                    {
                        "rateType": "ACCOUNT",
                        "ratedWeightMethod": "ACTUAL",
                        "totalDiscounts": 0.0,
                        "totalBaseCharge": 85.83,
                        "totalNetCharge": 90.12,
                        "totalNetFedExCharge": 90.12,
                        "shipmentRateDetail": {
                            "rateZone": "06",
                            "dimDivisor": 139,
                            "fuelSurchargePercent": 5.0,
                            "totalSurcharges": 4.29,
                            "totalFreightDiscount": 0.0,
                            "surCharges": [
                                {
                                    "type": "FUEL",
                                    "description": "Fuel Surcharge",
                                    "amount": 4.29
                                }
                            ],
                            "pricingCode": "PACKAGE",
                            "totalBillingWeight": {
                                "units": "LB",
                                "value": 2.0
                            },
                            "currency": "USD",
                            "rateScale": "1371"
                        },
                        "ratedPackages": [
                            {
                                "groupNumber": 0,
                                "effectiveNetDiscount": 0.0,
                                "packageRateDetail": {
                                    "rateType": "PAYOR_LIST_PACKAGE",
                                    "ratedWeightMethod": "ACTUAL",
                                    "baseCharge": 85.83,
                                    "netFreight": 85.83,
                                    "totalSurcharges": 4.29,
                                    "netFedExCharge": 90.12,
                                    "totalTaxes": 0.0,
                                    "netCharge": 90.12,
                                    "totalRebates": 0.0,
                                    "billingWeight": {
                                        "units": "LB",
                                        "value": 2.0
                                    },
                                    "totalFreightDiscounts": 0.0,
                                    "surcharges": [
                                        {
                                            "type": "FUEL",
                                            "description": "Fuel Surcharge",
                                            "amount": 4.29
                                        }
                                    ],
                                    "currency": "USD"
                                }
                            }
                        ],
                        "currency": "USD"
                    }
                ],
                "operationalDetail": {
                    "ineligibleForMoneyBackGuarantee": false,
                    "astraDescription": "STD OVR",
                    "airportId": "SWF",
                    "serviceCode": "05"
                },
                "signatureOptionType": "SERVICE_DEFAULT",
                "serviceDescription": {
                    "serviceId": "EP1000000005",
                    "serviceType": "STANDARD_OVERNIGHT",
                    "code": "05",
                    "names": [
                        {
                            "type": "long",
                            "encoding": "utf-8",
                            "value": "FedEx Standard Overnight®"
                        },
                        {
                            "type": "long",
                            "encoding": "ascii",
                            "value": "FedEx Standard Overnight"
                        },
                        {
                            "type": "medium",
                            "encoding": "utf-8",
                            "value": "FedEx Standard Overnight®"
                        },
                        {
                            "type": "medium",
                            "encoding": "ascii",
                            "value": "FedEx Standard Overnight"
                        },
                        {
                            "type": "short",
                            "encoding": "utf-8",
                            "value": "SOS"
                        },
                        {
                            "type": "short",
                            "encoding": "ascii",
                            "value": "SOS"
                        },
                        {
                            "type": "abbrv",
                            "encoding": "ascii",
                            "value": "SO"
                        }
                    ],
                    "serviceCategory": "parcel",
                    "description": "Standard Overnight",
                    "astraDescription": "STD OVR"
                }
            },
            {
                "serviceType": "FEDEX_2_DAY_AM",
                "serviceName": "FedEx 2Day® AM",
                "packagingType": "YOUR_PACKAGING",
                "ratedShipmentDetails": [
                    {
                        "rateType": "LIST",
                        "ratedWeightMethod": "ACTUAL",
                        "totalDiscounts": 0.0,
                        "totalBaseCharge": 45.88,
                        "totalNetCharge": 48.17,
                        "totalNetFedExCharge": 48.17,
                        "shipmentRateDetail": {
                            "rateZone": "06",
                            "dimDivisor": 139,
                            "fuelSurchargePercent": 5.0,
                            "totalSurcharges": 2.29,
                            "totalFreightDiscount": 0.0,
                            "surCharges": [
                                {
                                    "type": "FUEL",
                                    "description": "Fuel Surcharge",
                                    "amount": 2.29
                                }
                            ],
                            "pricingCode": "PACKAGE",
                            "totalBillingWeight": {
                                "units": "LB",
                                "value": 2.0
                            },
                            "currency": "USD",
                            "rateScale": "12"
                        },
                        "ratedPackages": [
                            {
                                "groupNumber": 0,
                                "effectiveNetDiscount": 0.0,
                                "packageRateDetail": {
                                    "rateType": "PAYOR_ACCOUNT_PACKAGE",
                                    "ratedWeightMethod": "ACTUAL",
                                    "baseCharge": 45.88,
                                    "netFreight": 45.88,
                                    "totalSurcharges": 2.29,
                                    "netFedExCharge": 48.17,
                                    "totalTaxes": 0.0,
                                    "netCharge": 48.17,
                                    "totalRebates": 0.0,
                                    "billingWeight": {
                                        "units": "LB",
                                        "value": 2.0
                                    },
                                    "totalFreightDiscounts": 0.0,
                                    "surcharges": [
                                        {
                                            "type": "FUEL",
                                            "description": "Fuel Surcharge",
                                            "amount": 2.29
                                        }
                                    ],
                                    "currency": "USD"
                                }
                            }
                        ],
                        "currency": "USD"
                    },
                    {
                        "rateType": "ACCOUNT",
                        "ratedWeightMethod": "ACTUAL",
                        "totalDiscounts": 0.0,
                        "totalBaseCharge": 45.88,
                        "totalNetCharge": 48.17,
                        "totalNetFedExCharge": 48.17,
                        "shipmentRateDetail": {
                            "rateZone": "06",
                            "dimDivisor": 139,
                            "fuelSurchargePercent": 5.0,
                            "totalSurcharges": 2.29,
                            "totalFreightDiscount": 0.0,
                            "surCharges": [
                                {
                                    "type": "FUEL",
                                    "description": "Fuel Surcharge",
                                    "amount": 2.29
                                }
                            ],
                            "pricingCode": "PACKAGE",
                            "totalBillingWeight": {
                                "units": "LB",
                                "value": 2.0
                            },
                            "currency": "USD",
                            "rateScale": "12"
                        },
                        "ratedPackages": [
                            {
                                "groupNumber": 0,
                                "effectiveNetDiscount": 0.0,
                                "packageRateDetail": {
                                    "rateType": "PAYOR_LIST_PACKAGE",
                                    "ratedWeightMethod": "ACTUAL",
                                    "baseCharge": 45.88,
                                    "netFreight": 45.88,
                                    "totalSurcharges": 2.29,
                                    "netFedExCharge": 48.17,
                                    "totalTaxes": 0.0,
                                    "netCharge": 48.17,
                                    "totalRebates": 0.0,
                                    "billingWeight": {
                                        "units": "LB",
                                        "value": 2.0
                                    },
                                    "totalFreightDiscounts": 0.0,
                                    "surcharges": [
                                        {
                                            "type": "FUEL",
                                            "description": "Fuel Surcharge",
                                            "amount": 2.29
                                        }
                                    ],
                                    "currency": "USD"
                                }
                            }
                        ],
                        "currency": "USD"
                    }
                ],
                "operationalDetail": {
                    "ineligibleForMoneyBackGuarantee": false,
                    "astraDescription": "2DAY AM",
                    "airportId": "SWF",
                    "serviceCode": "49"
                },
                "signatureOptionType": "SERVICE_DEFAULT",
                "serviceDescription": {
                    "serviceId": "EP1000000023",
                    "serviceType": "FEDEX_2_DAY_AM",
                    "code": "49",
                    "names": [
                        {
                            "type": "long",
                            "encoding": "utf-8",
                            "value": "FedEx 2Day® AM"
                        },
                        {
                            "type": "long",
                            "encoding": "ascii",
                            "value": "FedEx 2Day AM"
                        },
                        {
                            "type": "medium",
                            "encoding": "utf-8",
                            "value": "FedEx 2Day® AM"
                        },
                        {
                            "type": "medium",
                            "encoding": "ascii",
                            "value": "FedEx 2Day AM"
                        },
                        {
                            "type": "short",
                            "encoding": "utf-8",
                            "value": "E2AM"
                        },
                        {
                            "type": "short",
                            "encoding": "ascii",
                            "value": "E2AM"
                        },
                        {
                            "type": "abbrv",
                            "encoding": "ascii",
                            "value": "TA"
                        }
                    ],
                    "serviceCategory": "parcel",
                    "description": "2DAY AM",
                    "astraDescription": "2DAY AM"
                }
            },
            {
                "serviceType": "FEDEX_2_DAY",
                "serviceName": "FedEx 2Day®",
                "packagingType": "YOUR_PACKAGING",
                "ratedShipmentDetails": [
                    {
                        "rateType": "LIST",
                        "ratedWeightMethod": "ACTUAL",
                        "totalDiscounts": 0.0,
                        "totalBaseCharge": 38.9,
                        "totalNetCharge": 40.85,
                        "totalNetFedExCharge": 40.85,
                        "shipmentRateDetail": {
                            "rateZone": "06",
                            "dimDivisor": 139,
                            "fuelSurchargePercent": 5.0,
                            "totalSurcharges": 1.95,
                            "totalFreightDiscount": 0.0,
                            "surCharges": [
                                {
                                    "type": "FUEL",
                                    "description": "Fuel Surcharge",
                                    "amount": 1.95
                                }
                            ],
                            "pricingCode": "PACKAGE",
                            "totalBillingWeight": {
                                "units": "LB",
                                "value": 2.0
                            },
                            "currency": "USD",
                            "rateScale": "6068"
                        },
                        "ratedPackages": [
                            {
                                "groupNumber": 0,
                                "effectiveNetDiscount": 0.0,
                                "packageRateDetail": {
                                    "rateType": "PAYOR_ACCOUNT_PACKAGE",
                                    "ratedWeightMethod": "ACTUAL",
                                    "baseCharge": 38.9,
                                    "netFreight": 38.9,
                                    "totalSurcharges": 1.95,
                                    "netFedExCharge": 40.85,
                                    "totalTaxes": 0.0,
                                    "netCharge": 40.85,
                                    "totalRebates": 0.0,
                                    "billingWeight": {
                                        "units": "LB",
                                        "value": 2.0
                                    },
                                    "totalFreightDiscounts": 0.0,
                                    "surcharges": [
                                        {
                                            "type": "FUEL",
                                            "description": "Fuel Surcharge",
                                            "amount": 1.95
                                        }
                                    ],
                                    "currency": "USD"
                                }
                            }
                        ],
                        "currency": "USD"
                    },
                    {
                        "rateType": "ACCOUNT",
                        "ratedWeightMethod": "ACTUAL",
                        "totalDiscounts": 0.0,
                        "totalBaseCharge": 38.9,
                        "totalNetCharge": 40.85,
                        "totalNetFedExCharge": 40.85,
                        "shipmentRateDetail": {
                            "rateZone": "06",
                            "dimDivisor": 139,
                            "fuelSurchargePercent": 5.0,
                            "totalSurcharges": 1.95,
                            "totalFreightDiscount": 0.0,
                            "surCharges": [
                                {
                                    "type": "FUEL",
                                    "description": "Fuel Surcharge",
                                    "amount": 1.95
                                }
                            ],
                            "pricingCode": "PACKAGE",
                            "totalBillingWeight": {
                                "units": "LB",
                                "value": 2.0
                            },
                            "currency": "USD",
                            "rateScale": "6068"
                        },
                        "ratedPackages": [
                            {
                                "groupNumber": 0,
                                "effectiveNetDiscount": 0.0,
                                "packageRateDetail": {
                                    "rateType": "PAYOR_LIST_PACKAGE",
                                    "ratedWeightMethod": "ACTUAL",
                                    "baseCharge": 38.9,
                                    "netFreight": 38.9,
                                    "totalSurcharges": 1.95,
                                    "netFedExCharge": 40.85,
                                    "totalTaxes": 0.0,
                                    "netCharge": 40.85,
                                    "totalRebates": 0.0,
                                    "billingWeight": {
                                        "units": "LB",
                                        "value": 2.0
                                    },
                                    "totalFreightDiscounts": 0.0,
                                    "surcharges": [
                                        {
                                            "type": "FUEL",
                                            "description": "Fuel Surcharge",
                                            "amount": 1.95
                                        }
                                    ],
                                    "currency": "USD"
                                }
                            }
                        ],
                        "currency": "USD"
                    }
                ],
                "operationalDetail": {
                    "ineligibleForMoneyBackGuarantee": false,
                    "astraDescription": "E2",
                    "airportId": "SWF",
                    "serviceCode": "03"
                },
                "signatureOptionType": "SERVICE_DEFAULT",
                "serviceDescription": {
                    "serviceId": "EP1000000003",
                    "serviceType": "FEDEX_2_DAY",
                    "code": "03",
                    "names": [
                        {
                            "type": "long",
                            "encoding": "utf-8",
                            "value": "FedEx 2Day®"
                        },
                        {
                            "type": "long",
                            "encoding": "ascii",
                            "value": "FedEx 2Day"
                        },
                        {
                            "type": "medium",
                            "encoding": "utf-8",
                            "value": "FedEx 2Day®"
                        },
                        {
                            "type": "medium",
                            "encoding": "ascii",
                            "value": "FedEx 2Day"
                        },
                        {
                            "type": "short",
                            "encoding": "utf-8",
                            "value": "P-2"
                        },
                        {
                            "type": "short",
                            "encoding": "ascii",
                            "value": "P-2"
                        },
                        {
                            "type": "abbrv",
                            "encoding": "ascii",
                            "value": "ES"
                        }
                    ],
                    "serviceCategory": "parcel",
                    "description": "2Day",
                    "astraDescription": "E2"
                }
            },
            {
                "serviceType": "FEDEX_EXPRESS_SAVER",
                "serviceName": "FedEx Express Saver®",
                "packagingType": "YOUR_PACKAGING",
                "ratedShipmentDetails": [
                    {
                        "rateType": "LIST",
                        "ratedWeightMethod": "ACTUAL",
                        "totalDiscounts": 0.0,
                        "totalBaseCharge": 31.41,
                        "totalNetCharge": 32.98,
                        "totalNetFedExCharge": 32.98,
                        "shipmentRateDetail": {
                            "rateZone": "06",
                            "dimDivisor": 139,
                            "fuelSurchargePercent": 5.0,
                            "totalSurcharges": 1.57,
                            "totalFreightDiscount": 0.0,
                            "surCharges": [
                                {
                                    "type": "FUEL",
                                    "description": "Fuel Surcharge",
                                    "amount": 1.57
                                }
                            ],
                            "pricingCode": "PACKAGE",
                            "totalBillingWeight": {
                                "units": "LB",
                                "value": 2.0
                            },
                            "currency": "USD",
                            "rateScale": "7175"
                        },
                        "ratedPackages": [
                            {
                                "groupNumber": 0,
                                "effectiveNetDiscount": 0.0,
                                "packageRateDetail": {
                                    "rateType": "PAYOR_ACCOUNT_PACKAGE",
                                    "ratedWeightMethod": "ACTUAL",
                                    "baseCharge": 31.41,
                                    "netFreight": 31.41,
                                    "totalSurcharges": 1.57,
                                    "netFedExCharge": 32.98,
                                    "totalTaxes": 0.0,
                                    "netCharge": 32.98,
                                    "totalRebates": 0.0,
                                    "billingWeight": {
                                        "units": "LB",
                                        "value": 2.0
                                    },
                                    "totalFreightDiscounts": 0.0,
                                    "surcharges": [
                                        {
                                            "type": "FUEL",
                                            "description": "Fuel Surcharge",
                                            "amount": 1.57
                                        }
                                    ],
                                    "currency": "USD"
                                }
                            }
                        ],
                        "currency": "USD"
                    },
                    {
                        "rateType": "ACCOUNT",
                        "ratedWeightMethod": "ACTUAL",
                        "totalDiscounts": 0.0,
                        "totalBaseCharge": 31.41,
                        "totalNetCharge": 32.98,
                        "totalNetFedExCharge": 32.98,
                        "shipmentRateDetail": {
                            "rateZone": "06",
                            "dimDivisor": 139,
                            "fuelSurchargePercent": 5.0,
                            "totalSurcharges": 1.57,
                            "totalFreightDiscount": 0.0,
                            "surCharges": [
                                {
                                    "type": "FUEL",
                                    "description": "Fuel Surcharge",
                                    "amount": 1.57
                                }
                            ],
                            "pricingCode": "PACKAGE",
                            "totalBillingWeight": {
                                "units": "LB",
                                "value": 2.0
                            },
                            "currency": "USD",
                            "rateScale": "7175"
                        },
                        "ratedPackages": [
                            {
                                "groupNumber": 0,
                                "effectiveNetDiscount": 0.0,
                                "packageRateDetail": {
                                    "rateType": "PAYOR_LIST_PACKAGE",
                                    "ratedWeightMethod": "ACTUAL",
                                    "baseCharge": 31.41,
                                    "netFreight": 31.41,
                                    "totalSurcharges": 1.57,
                                    "netFedExCharge": 32.98,
                                    "totalTaxes": 0.0,
                                    "netCharge": 32.98,
                                    "totalRebates": 0.0,
                                    "billingWeight": {
                                        "units": "LB",
                                        "value": 2.0
                                    },
                                    "totalFreightDiscounts": 0.0,
                                    "surcharges": [
                                        {
                                            "type": "FUEL",
                                            "description": "Fuel Surcharge",
                                            "amount": 1.57
                                        }
                                    ],
                                    "currency": "USD"
                                }
                            }
                        ],
                        "currency": "USD"
                    }
                ],
                "operationalDetail": {
                    "ineligibleForMoneyBackGuarantee": false,
                    "astraDescription": "XS",
                    "airportId": "SWF",
                    "serviceCode": "20"
                },
                "signatureOptionType": "SERVICE_DEFAULT",
                "serviceDescription": {
                    "serviceId": "EP1000000013",
                    "serviceType": "FEDEX_EXPRESS_SAVER",
                    "code": "20",
                    "names": [
                        {
                            "type": "long",
                            "encoding": "utf-8",
                            "value": "FedEx Express Saver®"
                        },
                        {
                            "type": "long",
                            "encoding": "ascii",
                            "value": "FedEx Express Saver"
                        },
                        {
                            "type": "medium",
                            "encoding": "utf-8",
                            "value": "FedEx Express Saver®"
                        },
                        {
                            "type": "medium",
                            "encoding": "ascii",
                            "value": "FedEx Express Saver"
                        }
                    ],
                    "serviceCategory": "parcel",
                    "description": "Express Saver",
                    "astraDescription": "XS"
                }
            },
            {
                "serviceType": "FEDEX_GROUND",
                "serviceName": "FedEx Ground®",
                "packagingType": "YOUR_PACKAGING",
                "ratedShipmentDetails": [
                    {
                        "rateType": "LIST",
                        "ratedWeightMethod": "ACTUAL",
                        "totalDiscounts": 0.0,
                        "totalBaseCharge": 13.98,
                        "totalNetCharge": 16.57,
                        "totalNetFedExCharge": 16.57,
                        "shipmentRateDetail": {
                            "rateZone": "6",
                            "dimDivisor": 0,
                            "fuelSurchargePercent": 18.5,
                            "totalSurcharges": 2.59,
                            "totalFreightDiscount": 0.0,
                            "surCharges": [
                                {
                                    "type": "FUEL",
                                    "description": "Fuel Surcharge",
                                    "level": "PACKAGE",
                                    "amount": 2.59
                                }
                            ],
                            "totalBillingWeight": {
                                "units": "LB",
                                "value": 2.0
                            },
                            "currency": "USD"
                        },
                        "ratedPackages": [
                            {
                                "groupNumber": 0,
                                "effectiveNetDiscount": 0.0,
                                "packageRateDetail": {
                                    "rateType": "PAYOR_ACCOUNT_PACKAGE",
                                    "ratedWeightMethod": "ACTUAL",
                                    "baseCharge": 13.98,
                                    "netFreight": 13.98,
                                    "totalSurcharges": 2.59,
                                    "netFedExCharge": 16.57,
                                    "totalTaxes": 0.0,
                                    "netCharge": 16.57,
                                    "totalRebates": 0.0,
                                    "billingWeight": {
                                        "units": "LB",
                                        "value": 2.0
                                    },
                                    "totalFreightDiscounts": 0.0,
                                    "surcharges": [
                                        {
                                            "type": "FUEL",
                                            "description": "Fuel Surcharge",
                                            "level": "PACKAGE",
                                            "amount": 2.59
                                        }
                                    ],
                                    "currency": "USD"
                                }
                            }
                        ],
                        "currency": "USD"
                    },
                    {
                        "rateType": "ACCOUNT",
                        "ratedWeightMethod": "ACTUAL",
                        "totalDiscounts": 0.0,
                        "totalBaseCharge": 13.98,
                        "totalNetCharge": 16.57,
                        "totalNetFedExCharge": 16.57,
                        "shipmentRateDetail": {
                            "rateZone": "6",
                            "dimDivisor": 0,
                            "fuelSurchargePercent": 18.5,
                            "totalSurcharges": 2.59,
                            "totalFreightDiscount": 0.0,
                            "surCharges": [
                                {
                                    "type": "FUEL",
                                    "description": "Fuel Surcharge",
                                    "level": "PACKAGE",
                                    "amount": 2.59
                                }
                            ],
                            "totalBillingWeight": {
                                "units": "LB",
                                "value": 2.0
                            },
                            "currency": "USD"
                        },
                        "ratedPackages": [
                            {
                                "groupNumber": 0,
                                "effectiveNetDiscount": 0.0,
                                "packageRateDetail": {
                                    "rateType": "PAYOR_LIST_PACKAGE",
                                    "ratedWeightMethod": "ACTUAL",
                                    "baseCharge": 13.98,
                                    "netFreight": 13.98,
                                    "totalSurcharges": 2.59,
                                    "netFedExCharge": 16.57,
                                    "totalTaxes": 0.0,
                                    "netCharge": 16.57,
                                    "totalRebates": 0.0,
                                    "billingWeight": {
                                        "units": "LB",
                                        "value": 2.0
                                    },
                                    "totalFreightDiscounts": 0.0,
                                    "surcharges": [
                                        {
                                            "type": "FUEL",
                                            "description": "Fuel Surcharge",
                                            "level": "PACKAGE",
                                            "amount": 2.59
                                        }
                                    ],
                                    "currency": "USD"
                                }
                            }
                        ],
                        "currency": "USD"
                    }
                ],
                "operationalDetail": {
                    "ineligibleForMoneyBackGuarantee": false,
                    "astraDescription": "FXG",
                    "airportId": "SWF",
                    "serviceCode": "92"
                },
                "signatureOptionType": "SERVICE_DEFAULT",
                "serviceDescription": {
                    "serviceId": "EP1000000134",
                    "serviceType": "FEDEX_GROUND",
                    "code": "92",
                    "names": [
                        {
                            "type": "long",
                            "encoding": "utf-8",
                            "value": "FedEx Ground®"
                        },
                        {
                            "type": "long",
                            "encoding": "ascii",
                            "value": "FedEx Ground"
                        },
                        {
                            "type": "medium",
                            "encoding": "utf-8",
                            "value": "Ground®"
                        },
                        {
                            "type": "medium",
                            "encoding": "ascii",
                            "value": "Ground"
                        },
                        {
                            "type": "short",
                            "encoding": "utf-8",
                            "value": "FG"
                        },
                        {
                            "type": "short",
                            "encoding": "ascii",
                            "value": "FG"
                        },
                        {
                            "type": "abbrv",
                            "encoding": "ascii",
                            "value": "SG"
                        }
                    ],
                    "description": "FedEx Ground",
                    "astraDescription": "FXG"
                }
            }
        ],
        "quoteDate": "2024-01-27",
        "encoded": false
    }
}
EOD;
    // cspell:enable
    return json_decode($json);
  }

}
