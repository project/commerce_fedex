<?php

namespace Drupal\commerce_fedex_test;

use Drupal\commerce_fedex\FedExRequest;
use FedexRest\Services\Rates\CreateRatesRequest;

/**
 * Override request service to return a simulated rate request.
 */
class TestFedExRequest extends FedExRequest {

  /**
   * {@inheritdoc}
   */
  public function getRateRequest(array $configuration, array $wsdlOptions = [], $resetSoapClient = TRUE): CreateRatesRequest {
    return new TestRateRequest();
  }

}
