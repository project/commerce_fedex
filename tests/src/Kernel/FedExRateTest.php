<?php

namespace Drupal\Tests\commerce_fedex\Kernel;

use Drupal\commerce_fedex\FedExRequestInterface;
use Drupal\commerce_fedex\Plugin\Commerce\ShippingMethod\FedEx;
use Drupal\commerce_shipping\Entity\ShippingMethodInterface;
use Drupal\commerce_shipping\ShippingRate;
use FedexRest\Services\Ship\Type\PickupType;
use FedexRest\Services\Ship\Type\ServiceType;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * Tests the shipment order processor.
 *
 * @coversDefaultClass \Drupal\commerce_fedex\Plugin\Commerce\ShippingMethod\FedEx
 * @group commerce_fedex
 */
class FedExRateTest extends FedExKernelTestBase {

  use ProphecyTrait;

  /**
   * Covers ::calculateRates.
   */
  public function testAllInOneRates(): void {

    $shipments = $this->packerManager->packToShipments($this->order, $this->profile, []);
    $shipments = array_shift($shipments);
    $shipment = reset($shipments);
    $configuration = [
      'api_information' => [
        'api_key' => static::TEST_KEY,
        'api_password' => static::TEST_PASS,
        'account_number' => static::TEST_ACCOUNT_NUMBER,
        'mode' => 'test',
      ],
      'options' => [
        'packaging' => FedEx::PACKAGE_ALL_IN_ONE,
        'rate_request_type' => [FedExRequestInterface::RATE_REQUEST_TYPE_LIST],
        'pickup_type' => PickupType::_DROPOFF_AT_FEDEX_LOCATION,
        'insurance' => FALSE,
        'rate_multiplier' => 1.0,
        'round' => PHP_ROUND_HALF_UP,
        'log' => [],
      ],
      'plugins' => [],
      'default_package_type' => 'custom_box',
      'services' => [ServiceType::_FEDEX_GROUND],
    ];
    $shipping_method = $this->prophesize(ShippingMethodInterface::class);
    $shipping_method->id()->willReturn('123456789');

    $plugin = $this->shippingMethodManager->createInstance('fedex', $configuration);
    assert($plugin instanceof FedEx);
    $plugin->setParentEntity($shipping_method->reveal());
    $shipment->setPackageType($plugin->getDefaultPackageType());
    $rates = $plugin->calculateRates(reset($shipments));

    $this->assertCount(1, $rates);
    $ground_rate = reset($rates);
    assert($ground_rate instanceof ShippingRate);
    $this->assertEquals('16.57', $ground_rate->getAmount()->getNumber());
    $this->assertEquals('16.57', $ground_rate->getOriginalAmount()->getNumber());
    $this->assertEquals('123456789', $ground_rate->getShippingMethodId());

  }

}
