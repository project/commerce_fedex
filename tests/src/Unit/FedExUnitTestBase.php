<?php

namespace Drupal\Tests\commerce_fedex\Unit;

use Drupal\commerce_fedex_test\TestFedExRequest;
use Drupal\Tests\UnitTestCase;

/**
 * A test base class for FedEx Unit Tests.
 */
abstract class FedExUnitTestBase extends UnitTestCase {

  /**
   * The FedEx request.
   *
   * @var \Drupal\commerce_fedex_test\TestFedExRequest
   */
  protected $request;

  /**
   * The configuration array.
   *
   * @var array
   */
  protected $configuration;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    parent::setUp();
    $this->request = new TestFedExRequest();
    $this->configuration = [
      'api_information' => [
        'api_key' => 'test_key',
        'api_password' => 'test_password',
        'account_number' => '1234567',
        'mode' => 'test',
      ],
    ];
  }

}
