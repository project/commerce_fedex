<?php

namespace Drupal\Tests\commerce_fedex\Unit;

/**
 * Test Class for the Fedex Shipping Plugin.
 *
 * @coversDefaultClass \Drupal\commerce_fedex\FedExRequest
 * @group commerce_fedex
 */
class FedExRequestTest extends FedExUnitTestBase {

  public const RATE_SERVICE_SERVICE_ID = 'crs';
  public const RATE_SERVICE_MAJOR_VERSION = 20;

  /**
   * A FedExRequest object to test.
   *
   * @var \Drupal\commerce_fedex\FedExRequest
   */
  protected $request;

  /**
   * A sample configuration to use.
   *
   * @var array
   */
  protected $configuration;

  /**
   * @covers ::getRateRequest
   */
  public function testRateRequest(): void {

    $configuration = $this->configuration;
    $rate_request = $this->request->getRateRequest($configuration);
    $this->assertEquals('https://apis-sandbox.fedex.com', $rate_request->getApiUri());
  }

}
