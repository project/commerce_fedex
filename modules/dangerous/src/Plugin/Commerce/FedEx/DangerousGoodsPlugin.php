<?php

namespace Drupal\commerce_fedex_dangerous\Plugin\Commerce\FedEx;

use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_fedex\Plugin\Commerce\FedEx\FedExPluginBase;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\ShipmentItem;
use Drupal\Core\Form\FormStateInterface;
use FedexRest\Entity\DangerousGoodsDetail;
use FedexRest\Entity\Item;
use FedexRest\Entity\PackageSpecialServicesRequested;
use FedexRest\Services\Ship\Type\PackageSpecialServiceType;

/**
 * Provides the FedEx Dangerous Goods Service Plugin.
 *
 * @CommerceFedExPlugin(
 *   id = "dangerous",
 *   label = @Translation("FedEx Dangerous Goods"),
 *   options_label = @Translation("Dangerous Goods Shipment Options"),
 *   options_description = @Translation("Enter your global shipping options for Dangerous Goods shipments")
 * )
 */
class DangerousGoodsPlugin extends FedExPluginBase {
  public const NOT_DANGEROUS = 0;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['contact_number' => ''] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['contact_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Contact Number'),
      '#description' => $this->t('Enter the Phone number of your dangerous goods/hazardous materials contact person'),
      '#default_value' => $this->configuration['contact_number'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['contact_number'] = $form_state->getValue('contact_number');
  }

  /**
   * {@inheritdoc}
   */
  public function adjustPackage(Item $package, array $shipment_items, ShipmentInterface $shipment): Item {
    $status = $this->getDangerousStatus(reset($shipment_items));

    if ($status === static::NOT_DANGEROUS) {
      return $package;
    }

    $special_services_requested = $package->packageSpecialServices ?? NULL;
    if ($special_services_requested === NULL) {
      $special_services_requested = new PackageSpecialServicesRequested();
    }
    $special_services_requested->addToSpecialServiceTypes(PackageSpecialServiceType::_DANGEROUS_GOODS);
    $dangerous_goods_detail = $special_services_requested->dangerousGoodsDetail ?? NULL;
    if ($dangerous_goods_detail === NULL) {
      $dangerous_goods_detail = new DangerousGoodsDetail();
    }
    $dangerous_goods_detail->setAccessibility($status);
    $special_services_requested->setDangerousGoodsDetail($dangerous_goods_detail);
    $package->setPackageSpecialServices($special_services_requested);

    return $package;
  }

  /**
   * {@inheritdoc}
   */
  public function splitPackage(array $shipment_items, ShipmentInterface $shipment): array {
    $packages = [];
    foreach ($shipment_items as $shipment_item) {
      $packages[$this->getDangerousStatus($shipment_item)][] = $shipment_item;
    }
    return array_values($packages);
  }

  /**
   * Returns the DG status of an item.
   *
   * @param \Drupal\commerce_shipping\ShipmentItem $shipment_item
   *   The item to check.
   *
   * @return mixed
   *   The DG status.
   */
  protected function getDangerousStatus(ShipmentItem $shipment_item) {
    /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
    $order_item = $this->orderItemStorage->load($shipment_item->getOrderItemId());
    $purchased_entity = $order_item->getPurchasedEntity();
    assert($purchased_entity instanceof PurchasableEntityInterface);
    if (!$purchased_entity->hasField('fedex_dangerous_accessibility') || $purchased_entity->get('fedex_dangerous_accessibility')->isEmpty()) {
      return static::NOT_DANGEROUS;
    }
    return $purchased_entity->get('fedex_dangerous_accessibility')->value;
  }

}
